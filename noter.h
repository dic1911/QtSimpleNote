#ifndef NOTER_H
#define NOTER_H

#include "qtmetamacros.h"
#include <iostream>
#include <QObject>
#include <QDir>

class noter : public QObject
{
    Q_OBJECT
public:
    explicit noter(QObject *parent = nullptr);

    QDir qdir;

    Q_PROPERTY (int noteCount READ noteCount WRITE setNoteCount NOTIFY onNoteCountChanged)
    int _noteCount;
    int noteCount() { return _noteCount; }
    void setNoteCount(int noteCount) {
        _noteCount = noteCount;
        emit onNoteCountChanged(_noteCount);
    }

    Q_PROPERTY (QList<QString> noteNames READ noteNames WRITE setNoteNames NOTIFY onNoteNamesChanged)
    QList<QString> _noteNames;
    QList<QString>& noteNames() { return _noteNames; }
    void setNoteNames(QList<QString> noteNames) {
        _noteNames = noteNames;
        emit onNoteNamesChanged(_noteNames);
    }

    Q_PROPERTY (int currentIndex READ currentIndex WRITE setCurrentIndex NOTIFY onCurrentIndexChanged)
    int _currentIndex;
    int currentIndex() { return _currentIndex; }
    void setCurrentIndex(int currentIndex) {
        _currentIndex = currentIndex;
        emit onCurrentIndexChanged(_currentIndex);
    }

    Q_INVOKABLE bool addNote(int i);
    Q_INVOKABLE bool addNote(QString name);
    Q_INVOKABLE void saveNote(int i, QString content);
    Q_INVOKABLE QString loadNote(int i);
    Q_INVOKABLE void deleteNote(int i);
    Q_INVOKABLE bool renameNote(int i, QString newName);
private:
    int maxIndex = 0;
signals:
    Q_SIGNAL void onNoteCountChanged(const int& newNoteCount);
    Q_SIGNAL void onNoteNamesChanged(const QList<QString>& newNoteNames);
    Q_SIGNAL void onCurrentIndexChanged(const int& newCurrentIndex);
};


#endif // NOTER_H
