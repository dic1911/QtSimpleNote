import QtQuick
import QtQuick.Controls 2.15
import Noter 0.0

Window {
    width: 1200
    height: 800
    visible: true
    title: qsTr("SimpleNote")
    id: root

    property int noteCount: MNoter.noteCount
    property string currentFilename: ''
    property string currentContent: ''
    property bool isModified: false
    property int nextNoteIndex: -1

    function _timer() {
        return Qt.createQmlObject("import QtQuick 2.0; Timer {}", root);
    }
    function delay(delayTime, cb) {
        timer = _timer();
        timer.interval = delayTime;
        timer.repeat = false;
        timer.triggered.connect(cb);
        timer.start();
    }

    function switchNote(index) {
        if (root.title.indexOf('(*)') > -1) {
            dlgAskSaveBeforeSwitch.visible = true;
            nextNoteIndex = index;
        } else {
            switchNoteReal(index);
        }
    }

    function switchNoteReal(index) {
         console.log(index + " was clicked", MNoter.noteNames[index])
         MNoter.currentIndex = index;
         txtTip.visible = false;
         noteContent.visible = true;
         btnSave.enabled = true;
         btnDel.enabled = true;
         btnRename.enabled = true;
         txtFilename.enabled = true;
         noteContent.enabled = true;
         noteContent.text = MNoter.loadNote(index);
         root.currentFilename = MNoter.noteNames[index];
         root.title = qsTr("SimpleNote - " + MNoter.noteNames[index])
        root.isModified = false;
    }

    Button {
        id: btnAdd
        text: '+'
        width: 30

        onClicked: {
            dlgNewNote.visible = true;
            txtTip.visible = false;
        }
    }

    Button {
        id: btnSave
        text: ' Save '
        enabled: false

        anchors.left: btnAdd.right
        anchors.leftMargin: 10

        onClicked: {
            MNoter.saveNote(MNoter.currentIndex, noteContent.text);
            root.title = root.title.replace(' (*)', '');
        }
    }

    Button {
        id: btnDel
        text: ' Delete '
        enabled: false

        anchors.left: btnSave.right
        anchors.leftMargin: 10

        onClicked: {
            MNoter.deleteNote(MNoter.currentIndex);
        }
    }

    Rectangle {
        id: filenameContainer
        width: 150
        height: btnAdd.height
        border.color: 'gray'

        anchors.left: btnDel.right
        anchors.leftMargin: 15

        TextEdit {
            id: txtFilename
            enabled: false

            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: 5
            anchors.leftMargin: 3

            text: root.currentFilename
        }
    }

    Button {
        id: btnRename
        text: ' Rename '
        enabled: false

        anchors.left: filenameContainer.right
        anchors.leftMargin: 10

        onClicked: {
            let oldName = MNoter.noteNames[MNoter.currentIndex];
            if (MNoter.renameNote(MNoter.currentIndex, txtFilename.text))
                root.title = root.title.replace(oldName, MNoter.noteNames[MNoter.currentIndex]);
        }
    }

    Flickable {
        id: flickable
        focus: true
        anchors.topMargin: 10
        anchors.top: btnAdd.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        contentWidth: 120 * root.noteCount
        Keys.onLeftPressed: scrollBar.decrease()
        Keys.onRightPressed: scrollBar.increase()

        Row {
            id: noteList
            spacing: 15

            Repeater {
                model: MNoter.noteNames.length

                Rectangle {
                    width: 100
                    height: 75
                    color: 'lightgray'

                    MouseArea {
                        anchors.fill: parent

                        onClicked: {
                            root.switchNote(index)
                        }

                        Text {
                            anchors.centerIn: parent
                            text: MNoter.noteNames[index]
                        }
                    }
                }
            }
        }

        ScrollBar.horizontal: ScrollBar {
            id: scrollBar
            active: true
            minimumSize: 0.1
            anchors.bottom: flickable.bottom
            policy: ScrollBar.AlwaysOn
        }
    }

    Flickable {
        id: noteContainer
        clip: true

        width: parent.width

        anchors.top: btnAdd.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        anchors.topMargin: 100
        anchors.leftMargin: 15
        anchors.rightMargin: 15
        anchors.bottomMargin: 15

        function ensureVisible(r) {
            if (contentX >= r.x)
                contentX = r.x;
            else if (contentX+width <= r.x+r.width)
                contentX = r.x+r.width-width;
            if (contentY >= r.y)
                contentY = r.y;
            else if (contentY+height <= r.y+r.height)
                contentY = r.y+r.height-height;
        }


        TextEdit {
            id: noteContent

            anchors.fill: parent
            anchors.leftMargin: 5
            anchors.rightMargin: 5
//            width: root.width
            wrapMode: TextEdit.Wrap

            visible: false

            onCursorRectangleChanged: noteContainer.ensureVisible(cursorRectangle)

            onTextChanged: {
                if (root.title.indexOf(' - ') && !root.isModified) {
                    root.isModified = true;
                    root.title += ' (*)'
                }
            }
        }

        Text {
            id: txtTip
            anchors.centerIn: parent

            text: 'Please select a note from above, or add a new note with \'+\' button'
        }

        ScrollBar {
                id: vbar
                hoverEnabled: true
                active: hovered || pressed
                orientation: Qt.Vertical
                size: noteContainer.height / noteContent.height
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.bottom: parent.bottom
            }

            ScrollBar {
                id: hbar
                hoverEnabled: true
                active: hovered || pressed
                orientation: Qt.Horizontal
                size: noteContainer.width / noteContent.width
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
            }
    }

    Rectangle {
        id: dlgNewNote
        visible: false
        width: 300
        height: 60

        anchors { leftMargin: -6; topMargin: -6; rightMargin: -8; bottomMargin: -8 }
        anchors.centerIn: parent

        border.width: 3
        border.color: 'gray'

        Rectangle {
            id: newFilenameContainer
            width: 200
            height: parent.height - 20

            border.color: 'gray'

            anchors.top: parent.top
            anchors.topMargin: ((dlgNewNote.height - newFilenameContainer.height) / 2)
            anchors.left: parent.left
            anchors.leftMargin: 10

            TextEdit {
                id: txtNewFilename

                anchors.top: parent.top
                anchors.left: parent.left
                anchors.topMargin: ((dlgNewNote.height - newFilenameContainer.height) / 2)
                anchors.leftMargin: 3

                text: "new_note.txt"

                onTextChanged: {
                    btnCreateNote.text = ' Create ';
                    btnCreateNote.enabled = true;
                }
            }
        }

        Button {
            id: btnCreateNote
            text: ' Create '

            anchors.top: parent.top
            anchors.topMargin: ((dlgNewNote.height - height) / 2)
            anchors.left: newFilenameContainer.right
            anchors.leftMargin: 5

            onClicked: {
                console.log("create note dialog:", txtNewFilename.text)
                if (!MNoter.addNote(txtNewFilename.text)) {
                    btnCreateNote.text = 'File Exists!';
                    btnCreateNote.enabled = false;
                } else {
                    dlgNewNote.visible = false;
                }
            }
        }

        Button {
            id: btnCancelCreateNote
            text: 'X'
            width: 30

            anchors.left: parent.right
            anchors.leftMargin: 5

            onClicked: {
                parent.visible = false;
            }
        }
    }

    Rectangle {
        id: dlgAskSaveBeforeSwitch
        visible: false
        width: 240
        height: 100

        anchors { leftMargin: -6; topMargin: -6; rightMargin: -8; bottomMargin: -8 }
        anchors.centerIn: parent

        border.width: 3
        border.color: 'gray'

        Text {
            id: txtAskSaveBeforeSwitch
            width: parent.width
            wrapMode: Text.Wrap

            anchors {
                top: parent.top
                topMargin: 5
                left: parent.left
                leftMargin: 5
                right: parent.right
                rightMargin: 5
            }

            text: 'You have unsaved changes, wanna save before you load another file?'
        }

        Rectangle {
            id: saveCancelBtnContainer

            anchors.top: txtAskSaveBeforeSwitch.bottom
            anchors.topMargin: ((dlgAskSaveBeforeSwitch.height - txtAskSaveBeforeSwitch.height - 5) / 2)
            anchors.left: dlgAskSaveBeforeSwitch.left
            anchors.leftMargin: (dlgAskSaveBeforeSwitch.width - btnSaveDlg.width - btnCancelDlg.width - 20) / 2
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10

            Button {
                id: btnSaveDlg
                text: ' Save '

                anchors.right: saveCancelBtnContainer.left
                anchors.rightMargin: 20

                onClicked: {
                    console.log("ask save note dialog - save")
                    MNoter.saveNote(MNoter.currentIndex, noteContent.text);
                    dlgAskSaveBeforeSwitch.visible = false;
                    root.switchNoteReal(root.nextNoteIndex);

                }
            }

            Button {
                id: btnCancelDlg
                text: ' Cancel '

                anchors.left: btnSaveDlg.right
                anchors.leftMargin: 20
                anchors.right: saveCancelBtnContainer.right
                anchors.rightMargin: 20


                onClicked: {
                    console.log("ask save note dialog - cancel")
                    dlgAskSaveBeforeSwitch.visible = false;
                    root.switchNoteReal(root.nextNoteIndex);
                }
            }
        }
    }
}
