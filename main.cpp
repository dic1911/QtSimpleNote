#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <filesystem>

#include "noter.h"


int main(int argc, char *argv[])
{

    QGuiApplication app(argc, argv);
    noter noter;

    if (!std::filesystem::is_directory("data")) {
        if (std::filesystem::exists("data")) {
            std::filesystem::remove("data");
        }
        std::filesystem::create_directory("data");
    }

    qmlRegisterSingletonInstance("Noter", 0, 0, "MNoter", &noter);

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/note/main.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
