#include "noter.h"

#include <QDir>
#include <QDirIterator>
#include <iostream>
#include <fstream>
#include <QString>
#include <QString>



noter::noter(QObject *parent)
    : QObject{parent}
{
    QDirIterator it(qdir.absolutePath() + "/data");
    int i = 0;
    int currentIndex = 0;
    while(it.hasNext()) {
        QFile f(it.next());

        auto path = std::filesystem::path(f.fileName().toStdString());
        auto filename = path.filename().generic_string();

//        bool valid = true;
//        for (int index = 0; index < filename.length(); index++) {
//            if (filename[index] < 48 || filename[index] > 57) {
//                valid = false;
//                break;
//            }
//        }
//        if (!valid) continue;
        if (filename[0] == '.')
            continue;

        try {
            currentIndex = std::stoi(filename);
        } catch(std::exception e) {}

        _noteNames.push_back(QString::fromStdString(filename));
        std::sort(_noteNames.begin(), _noteNames.end());
        if (maxIndex < currentIndex)
            maxIndex = currentIndex;
        ++i;

        std::cout << i << "\t" << f.fileName().toStdString() << std::endl;
    }

    setNoteCount(i);
    std::cout << "finished initializing noter." << std::endl;
}

bool noter::addNote(int i) {
    // last one isn't the last one on linux
    int name = ++maxIndex;//(_noteNames.size() != 0 ?_noteNames.last() + 1 : 0);
    std::string path = qdir.absolutePath().toStdString() + "/data/" + std::to_string(name);
    if (std::filesystem::exists(path)) return false;
    std::cout << "creating new note: " << path << std::endl;
    std::ofstream os(path);
    os.close();
    _noteNames.push_back(QString::fromStdString(std::to_string(name)));
    emit onNoteNamesChanged(_noteNames);
    return true;
}

bool noter::addNote(QString name) {
    std::string path = qdir.absolutePath().toStdString() + "/data/" + name.toStdString();
    if (std::filesystem::exists(path)) return false;
    std::cout << "creating new note: " << path << std::endl;
    std::ofstream os(path);
    os.close();
    _noteNames.push_back(name);
    emit onNoteNamesChanged(_noteNames);
    return true;
}

void noter::saveNote(int i, QString content) {
    std::string path = qdir.absolutePath().toStdString() + "/data/" + _noteNames[i].toStdString();
    std::cout << "saving note: " << path << std::endl;
    std::ofstream os(path);
    os << content.toStdString();
    // os.close();
}

QString noter::loadNote(int i) {
    std::string path = qdir.absolutePath().toStdString() + "/data/" + _noteNames[i].toStdString();
    std::ifstream is(path);
    std::string str(std::istreambuf_iterator<char>(is), {});
    return QString::fromStdString(str);
}

void noter::deleteNote(int i) {
    std::string path = qdir.absolutePath().toStdString() + "/data/" + _noteNames[i].toStdString();
    std::filesystem::remove(path);
    _noteNames.removeAt(i);
    emit onNoteNamesChanged(_noteNames);
}

bool noter::renameNote(int i, QString newName) {
    std::string path0 = qdir.absolutePath().toStdString() + "/data/" + _noteNames[i].toStdString();
    std::string path1 = qdir.absolutePath().toStdString() + "/data/" + newName.toStdString();
    if (std::filesystem::exists(path1))
        return false;
    std::filesystem::rename(path0, path1);
    _noteNames[i] = newName;
    emit onNoteNamesChanged(_noteNames);
    return true;
}
