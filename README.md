# SimpleNote

---

## Feature

 - Support multiple notes
 - Add/Rename/Delete functionality builtin
 - Mark at window title when file is modified and unsaved
 - Prompt to save if there's unsaved change when switching note

---

## Building

 - Ensure Qt 6, gcc, cmake and make is installed
 - Run `cmake . && make` in this repo
 - ???
 - Profit?
